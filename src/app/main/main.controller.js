(function() {
  'use strict';

  angular
    .module('myWeatherApp')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($timeout, webDevTec, toastr, $location, $scope, $http, $state) {
    var vm = this;

    $scope.date = new Date();

    $scope.position = navigator.geolocation.getCurrentPosition(
      function(position) {
        console.log(position.coords.latitude, position.coords.longitude);
        getWeather(position.coords.latitude,
          position.coords.longitude);
      });

    function getWeather(lat, lon) {
      $http.get(
          'http://api.openweathermap.org/data/2.5/weather?lat=' +
          lat + '&lon=' +
          lon + '&units=metric&APPID=53f9d8e4213222cf517d86dc406d67fc')
        .then(
          function(data){
            vm.weather = data;
            console.log(data);
            console.log(vm.weather);
          },
          function(error){
            if((lat == null) || (lon == null)) {
              vm.error = 'Oops, location not found..'
            }
            else if(error.data == null) {
              vm.error = 'Oops, something went wrong..'
            } else if (error.status == 408){
                vm.error = 'Oops, Network Error..'
            }
            vm.errorStatus = error.status;
          });
    }

    $scope.reloadRoute = function() {
      $state.reload();
    }

    function getDayNight() {
  var currentTime = new date();
    }

  }
})();
