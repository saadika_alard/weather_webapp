(function() {
  'use strict';

  angular
    .module('myWeatherApp')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
