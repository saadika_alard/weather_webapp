/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('myWeatherApp')
    .constant('malarkey', malarkey)
    .constant('moment', moment);

})();
